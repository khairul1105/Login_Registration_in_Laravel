@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">

                        <a href="/home">Dashboard</a> |
                        <a href="{{ url('/user/add') }}">Add New User</a> |
                        <a href="{{ url('/users') }}">All User</a>

                    </div>

                    <table border="1" class="table table-bordered table-responsive" bgcolor="#e6e6fa">

                        <thead>
                        <tbody>
                        <tr bgcolor="#5f9ea0">
                            <td><font color="#fff"><strong>ID</strong></font>

                            </td>
                            <td><font color="#fff"><strong>Name</strong></font></td>
                            <td><font color="#fff"><strong>Email</strong></font></td>
                            <td><font color="#fff"><strong>First Name</strong></font></td>
                            <td><font color="#fff"><strong>Last Name</strong></font></td>
                        </tr>
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->profile->first_name }}</td>
                            <td>{{ $user->profile->last_name }} </td>
                        </tr>
                        <tr bgcolor="#5f9ea0">

                            <td><font color="#fff"><strong>Gender</strong></font></td>
                            <td><font color="#fff"><strong>Fathers Name</strong></font></td>
                            <td><font color="#fff"><strong>Mothers Name</strong></font></td>
                            <td><font color="#fff"><strong>Mobile No</strong></font></td>
                            <td><font color="#fff"><strong>Address</strong></font></td>
                        </tr>
                        <tr>
                            <td>{{ $user->profile->gender }}</td>
                            <td>{{ $user->profile->father_name }}</td>
                            <td>{{ $user->profile->mother_name }}</td>
                            <td>{{ $user->profile->mobile }}</td>
                            <td>{{ $user->profile->address }} </td>
                        </tr>

                        <tr bgcolor="#5f9ea0">

                            <td><font color="#fff"><strong>Facebook</strong></font></td>
                            <td><font color="#fff"><strong>Website</strong></font></td>
                            <td><font color="#fff"><strong>Git</strong></font></td>
                            <td><font color="#fff"><strong>Linkedin</strong></font></td>
                            <td><font color="#fff"><strong>Action</strong></font></td>

                        </tr>
                        <tr>
                            <td>{{ $user->profile->facebook }}</td>
                            <td>{{ $user->profile->website }}</td>
                            <td>{{ $user->profile->git }}</td>
                            <td>
                                @if(!empty($user->profile->linkedin))
                                    {{ $user->profile->linkedin }}
                                @endif
                            </td>
                            <td>
                                <a href="{{ url('/user/'. $user->id. '/edit') }}">Edit</a>

                                <a href="{{ url('/user/'. $user->id. '/delete') }}"> | delete</a>
                            </td>
                        </tr>



                        </tbody>
                        </thead>

                    </table>


                </div>
            </div>
        </div>
    </div>
@endsection
