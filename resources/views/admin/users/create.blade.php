@extends('layouts.app')

@section('content')
    <div class="col-md-3">

    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                {!! Form::open(['url' => 'user/store', 'method' =>'post']) !!}

                <div class="form-group">
                    {!! Form::label('Name') !!}
                    {!! Form::text('name', null, ['class'=> 'form-control', 'placeholder' => 'Ex:John']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('Email') !!}
                    {!! Form::text('email', null, ['class'=> 'form-control', 'placeholder' => 'Ex:example@example.com']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('Password') !!}
                    {!! Form::password('password', null, ['class'=> 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('First_name') !!}
                    {!! Form::text('first_name', null, ['class'=> 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('Last_name') !!}
                    {!! Form::text('last_name', null, ['class'=> 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('Gender') !!}
                    {!! Form::text('gender', null, ['class'=> 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('Father_name') !!}
                    {!! Form::text('father_name', null, ['class'=> 'form-control']) !!}
                </div>


                <div class="form-group">
                    {!! Form::label('Mother_name') !!}
                    {!! Form::text('mother_name', null, ['class'=> 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('Mobile') !!}
                    {!! Form::number('mobile', null, ['class'=> 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('Address') !!}
                    {!! Form::text('address', null, ['class'=> 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('Facebook') !!}
                    {!! Form::url('facebook', null, ['class'=> 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('Website') !!}
                    {!! Form::url('website', null, ['class'=> 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('Git') !!}
                    {!! Form::url('git', null, ['class'=> 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('Linkedin') !!}
                    {!! Form::url('linkedin', null, ['class'=> 'form-control']) !!}
                </div>



                <div class="form-group">
                    {!! Form::submit('Submit', null, ['class'=> 'form-control']) !!}
                </div>


                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection