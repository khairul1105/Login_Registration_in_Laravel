@extends('layouts.app')

@section('content')
    <div class="col-md-3">

    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                {!! Form::open(['url' => 'user/'.$user->id.'/update', 'method' =>'post']) !!}
                <div class="form-group">
                    {!! Form::label('Name') !!}
                    {!! Form::text('name',  $user->name , ['class'=> 'form-control', 'placeholder' => 'Ex:Suhag']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('Email') !!}
                    {!! Form::text('email', $user->email, ['class'=> 'form-control', 'placeholder' => 'Ex:example@gmail.com']) !!}
                </div>


                <div class="form-group">
                    {!! Form::label('first_name') !!}
                    {!! Form::text('first_name', $user->profile->first_name , ['class'=> 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('last_name') !!}
                    {!! Form::text('last_name', $user->profile->last_name , ['class'=> 'form-control']) !!}
                </div>


                <div class="form-group">
                    {!! Form::label('gender') !!}
                    {!! Form::text('gender', $user->profile->gender , ['class'=> 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('father_name') !!}
                    {!! Form::text('father_name', $user->profile->father_name , ['class'=> 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('mother_name') !!}
                    {!! Form::text('mother_name', $user->profile->mother_name , ['class'=> 'form-control']) !!}
                </div>


                <div class="form-group">
                    {!! Form::label('mobile') !!}
                    {!! Form::text('mobile', $user->profile->mobile, ['class'=> 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('address') !!}
                    {!! Form::text('address', $user->profile->address, ['class'=> 'form-control']) !!}
                </div>


                <div class="form-group">
                    {!! Form::label('facebook') !!}
                    {!! Form::url('facebook', $user->profile->facebook , ['class'=> 'form-control']) !!}
                </div>


                <div class="form-group">
                    {!! Form::label('website') !!}
                    {!! Form::url('website', $user->profile->website, ['class'=> 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('git') !!}
                    {!! Form::url('git', $user->profile->git, ['class'=> 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('linkedin') !!}
                    {!! Form::url('linkedin', $user->profile->linkedin, ['class'=> 'form-control']) !!}
                </div>




                <div class="form-group">
                    {!! Form::submit('Submit', null, ['class'=> 'form-control']) !!}
                </div>


                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection