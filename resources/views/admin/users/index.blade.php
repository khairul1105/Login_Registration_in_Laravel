@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">

                        <a href="/home">Dashboard</a> |
                        <a href="{{ url('/user/add') }}">Add New User</a>

                    </div>

                    <table border="1" class="table table-bordered " bgcolor="#e6e6fa">
                        <thead>
                        <tbody>
                        <tr bgcolor="#5f9ea0">

                            <td><font color="#fff"><strong> ID</strong></font></td>
                            <td><font color="#fff"><strong>Name</strong></font></td>
                            <td><font color="#fff"><strong>Email</strong></font></td>
                            <td><font color="#fff"><strong>Fathers Name</strong></font></td>
                            <td><font color="#fff"><strong>Mothers Name</strong></font></td>
                            <td><font color="#fff"><strong>Address</strong></font></td>
                            <td><font color="#fff"><strong>Mobile No</strong></font></td>
                            <td><font color="#fff"><strong>Action</strong></font></td>
                        <tr/>
                        </thead>
                        @foreach($users as $user)

                            <tr bgcolor="#e6e6fa">
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>


                                <td>
                                    @if(!empty($user->profile->father_name))
                                        {{ $user->profile->father_name }}
                                    @endif
                                </td>

                                <td>
                                    @if(!empty($user->profile->mother_name))
                                        {{ $user->profile->mother_name }}
                                    @endif
                                </td>

                                <td>
                                    @if(!empty($user->profile->address))
                                        {{ $user->profile->address }}
                                    @endif
                                </td>

                                <td>
                                    @if(!empty($user->profile->mobile_no))
                                        {{ $user->profile->mobile_no }}
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ url('/user/'. $user->id) }}">View</a>
                                    <a href="{{ url('/user/'. $user->id. '/edit') }}"> | Edit</a>
                                    | Delete
                                </td>

                            </tr>
                            </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
