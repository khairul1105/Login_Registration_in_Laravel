<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Profile;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $allUsers = User::with('profile')->get();

        return view('admin.users.index', ['users' => $allUsers]);

    }


    public function create()
    {
        return view('admin.users.create');

    }


    public function store(Request $request)
    {
        $data = $request->only('name', 'email');
        $data['password'] = password_hash($request->password, PASSWORD_BCRYPT);
        $user = User::create($data);

        $profiledata = $request->only('first_name', 'last_name', 'gender', 'father_name', 'mother_name', 'mobile', 'address', 'facebook', 'website', 'git', 'linkedin');
        $user->profile()->create($profiledata);

        Session::flash('msg', 'Data Successfully Updated');
        return redirect('/users');
    }

    public function show($id)
    {
        $user = User::find($id);
        return view('admin.users.view', ['user' => $user]);
    }


    public function edit($id)
    {

        $user = User::findorfail($id);
        return view('admin.users.edit', ['user' => $user]);

    }


    public function update(Request $request, $id)
    {

        $user = User::findorfail($id);
        $data = $request->only('name', 'email');
        $user->update($data);

        $profiledata = $request->only('first_name', 'last_name', 'gender', 'father_name', 'mother_name', 'mobile', 'address', 'facebook', 'website', 'git', 'linkedin');
        $user->profile()->update($profiledata);

        Session::flash('msg', 'Data Successfully Updated');
        return redirect('/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
