<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = ['first_name', 'last_name', 'gender', 'father_name', 'mother_name', 'mobile', 'address', 'facebook', 'website', 'git', 'linkedin'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
