<?php
$factory->define(App\Profile::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'gender' => $faker->randomElement($array=array('male','female')),
        'father_name' => $faker->name,
        'mother_name' => $faker->name,
        'mobile' => $faker->phoneNumber,
        'address' => $faker->address,
        'facebook' => 'http://facebook.com/' . $faker->unique()->firstName,
        'website' => 'http://google.com/' . $faker->unique()->firstName,
        'git' => 'http://git.com/' . $faker->unique()->firstName,
        'linkedin' => 'http://linkedin.com/' . $faker->unique()->firstName,


    ];
});